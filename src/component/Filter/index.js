import React, { useState, useEffect } from 'react';
import { Grid, FormControlLabel, FormGroup } from '@mui/material';
import { Divider, Checkbox } from 'antd';
import { UpOutlined, DownOutlined } from '@ant-design/icons';
import { contentQuotesLinter } from '@ant-design/cssinjs/lib/linters';




const Filter = (props) => {
    const [ukuran, setUkuran] = useState([]);
    const [warna, setWarna] = useState([
        {
            label: "Merah", value: "merah", color: "#FF2536", isActive: false
        }, {
            label: "Hitam", value: "hitam", color: "#000000", isActive: false
        }, {
            label: "Putih", value: "putih", color: "#ffffff", isActive: false
        }, {
            label: "Abu-Abu", value: "abu", color: "#626067", isActive: false
        }, {
            label: "Hijau", value: "hijau", color: "#5DB63E", isActive: false
        }, {
            label: "Biru", value: "biru", color: "#1C5CD8", isActive: false
        }, {
            label: "Biru Muda", value: "birumuda", color: "#C9E0FF", isActive: false
        }, {
            label: "Krem", value: "krem", color: "#FBE2C5", isActive: false
        }, {
            label: "Kuning", value: "kuning", color: "#FFE86F", isActive: false
        }, {
            label: "Oranye", value: "oranye", color: "#FF8B37", isActive: false
        }, {
            label: "Ungu", value: "ungu", color: "#A44ED9", isActive: false
        },

    ]);
    const [brand, setBrand] = useState([]);
    const [color, setColor] = useState();
    const [showUkuran, setShowUkuran] = useState(false);
    const [showWarna, setShowWarna] = useState(true);
    const [showBrand, setShowBrand] = useState(true);

    const dataUkuran = [
        {
            label: "XS", value: "xs"
        },
        {
            label: "S", value: "s"
        },
        {
            label: "M", value: "m"
        },
        {
            label: "L", value: "l"
        },
        {
            label: "XL", value: "xl"
        }
    ]
    const dataBrand = [
        {
            label: "3Second", value: "3second"
        },
        {
            label: "ADIDAS", value: "adidas"
        },
        {
            label: "Abercrombie&Firch", value: "abercrombie"
        },
        {
            label: "BOSS", value: "boss"
        },
        {
            label: "Celciusmen", value: "celciusmen"
        },
        {
            label: "H&M", value: "hnm"
        },
        {
            label: "Hollister", value: "hollister"
        },
        {
            label: "Jack & Jones", value: "jack"
        },
        {
            label: "Levi's", value: "levis"
        },
        {
            label: "Tolliver", value: "tolliver"
        },
        {
            label: "Trendyol", value: "trendyol"
        },
        {
            label: "Under Armour", value: "underarmour"
        },
        {
            label: "VANS", value: "vans"
        }
    ]

    const onChangeUkuran = (e) => {
        setUkuran(e)
    }

    const onChangeWarna = (e) => {
        setWarna(e)
    }

    const onChangeBrand = (e) => {
        setBrand(e)
    }

    const handleClick = (color, value) => {
        props.setColor(value)
        setColor(value)
        var newData = warna.map(e => {
            if (e.color == color) {
                e.isActive = !e.isActive
            }
            return e
        })
        setWarna(newData)
    }


    return (
        <>
            <div style={{ padding: "20px", boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)", height: "auto" }}>
                <Grid container>
                    <span style={{ fontWeight: "bold" }}>Filter</span>
                    {
                        color ? <span style={{ marginLeft: "80px", color: "red", cursor: "pointer" }} onClick={() => {
                            window.location.reload(false)
                        }}>Hapus Semua</span>
                            : null
                    }

                    <Divider style={{ margin: "10px 0" }} />
                    <div style={{ display: "flex", flexDirection: "column" }}>
                        <span style={{ fontWeight: "bold" }}>Ukuran
                            {
                                showUkuran ? <UpOutlined style={{ fontSize: "12px", marginLeft: "150px" }} onClick={() => setShowUkuran(!showUkuran)} /> : <DownOutlined style={{ fontSize: "12px", marginLeft: "150px" }} onClick={() => setShowUkuran(!showUkuran)} />

                            }
                        </span>
                        {
                            showUkuran ?
                                <div style={{ margin: "10px 150px 10px 10px" }}>
                                    <FormGroup>
                                        <Checkbox.Group
                                            value={ukuran}
                                            onChange={onChangeUkuran}
                                            options={dataUkuran}
                                        />
                                    </FormGroup>
                                </div>
                                : null
                        }
                        <span style={{ fontWeight: "bold", marginTop: "10px" }}>Warna
                            {
                                showWarna ? <UpOutlined style={{ fontSize: "12px", marginLeft: "157px" }} onClick={() => setShowWarna(!showWarna)} /> : <DownOutlined style={{ fontSize: "12px", marginLeft: "157px" }} onClick={() => setShowWarna(!showWarna)} />

                            }
                        </span>
                        {
                            showWarna ?
                                <div style={{ marginRight: "80px" }}>
                                    {
                                        showWarna ?
                                            <>
                                                {warna.map((data, index) => (
                                                    <div onClick={() => handleClick(data.color, data.value)} style={{ border: '1px solid #BCBCBC', padding: "7px", width: "auto", float: "left", margin: "10px 10px 5px 5px", borderRadius: "3px", cursor: "pointer", backgroundColor: data.isActive ? "#BCBCBC" : "#FFFFFF" }} key={index}>
                                                        <div style={{ backgroundColor: data.color, width: "15px", height: "15px", border: '1px solid #BCBCBC', borderRadius: "3px", float: "left", marginRight: "5px" }}></div>
                                                        <span>{data.label}</span>
                                                    </div>
                                                ))}
                                            </>
                                            : null

                                    }
                                </div>
                                : null
                        }
                        <span style={{ fontWeight: "bold", marginTop: "10px" }}>Brand
                            {
                                showBrand ? <UpOutlined style={{ fontSize: "12px", marginLeft: "160px" }} onClick={() => setShowBrand(!showBrand)} /> : <DownOutlined style={{ fontSize: "12px", marginLeft: "160px" }} onClick={() => setShowBrand(!showBrand)} />

                            }
                        </span>
                        {
                            showBrand ?
                                <Checkbox.Group
                                    value={brand}
                                    onChange={onChangeBrand}
                                    options={dataBrand}
                                    style={{ margin: "10px 180px 10px 10px", fontWeight: "normal" }}
                                />
                                :
                                null
                        }
                    </div>
                </Grid>
            </div >



        </>
    )
}

export default Filter;
