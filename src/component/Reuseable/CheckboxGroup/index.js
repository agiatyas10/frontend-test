// main
import React from 'react';
import PropTypes from 'prop-types';

// libraries
import { Checkbox } from 'antd';

const CheckboxGroup = (props) => {

    const { value, onChange, options, type, width, margin } = props;

    return (
        <Checkbox.Group
            value={value}
            onChange={onChange}
            options={options}
        />
    );
};

CheckboxGroup.propTypes = {
    value: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    options: PropTypes.array,
    style: PropTypes.object,
    type: PropTypes.string,
    width: PropTypes.string,
    margin: PropTypes.string
};

CheckboxGroup.defaultProps = {
    options: [],
    type: 'checkbox',
    width: 'initial',
    margin: '0 30px 0 0'
};

export default CheckboxGroup;