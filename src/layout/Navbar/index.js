import React, { useState, useEffect } from 'react';
import { Input } from 'antd';
import { Grid } from '@mui/material';
import { SearchOutlined, ShoppingCartOutlined, UserOutlined } from '@ant-design/icons';

import Logo from "../../asset/image/TeeCommerce.svg";

const Navbar = props => {

    return (
        <>
            <div style={{ padding: "30px 35px", boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)" }}>
                <Grid container>
                    <Grid item lg={2}>
                        <img alt="logo" src={Logo} />
                    </Grid>
                    <Grid item lg={1}></Grid>
                    <Grid item lg={1}>
                        <a href="#" onClick={() => props.setType("PRIA")} style={{ cursor: "pointer" }}>
                            <span>PRIA </span>
                        </a>
                    </Grid>
                    <Grid item lg={1}>
                        <a href="#" onClick={() => props.setType("WANITA")} style={{ cursor: "pointer" }}>
                            <span>WANITA</span>
                        </a>
                    </Grid>
                    <Grid item lg={1}>
                        <a href="#" onClick={() => props.setType("ANAK-ANAK")} style={{ cursor: "pointer" }}>
                            <span>ANAK-ANAK</span>
                        </a>
                    </Grid>
                    <Grid item lg={3}></Grid>
                    <Grid item lg={3}>
                        <SearchOutlined style={{ fontSize: 20 }} />
                        <Input bordered={false} style={{ width: "180px", marginTop: "-20", textDecoration: "underline" }} />
                        <ShoppingCartOutlined style={{ fontSize: 20, marginRight: "25px" }} />
                        <UserOutlined style={{ fontSize: 20 }} />
                    </Grid>

                </Grid>
            </div>



        </>
    )
}

export default Navbar;
