import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Button, Skeleton, Radio, Pagination } from 'antd';
import { Grid, Modal, Box, Typography } from '@mui/material';
import { SortAscendingOutlined, ShoppingCartOutlined } from '@ant-design/icons';

import Filter from '../../component/Filter';
import Banner from '../../asset/image/bannerCatalog.png';
import BannerWoman from '../../asset/image/bannerWoman.png';
import BannerKids from '../../asset/image/bannerKids.png';

import Image1 from '../../asset/image/image11.png';
import Image2 from '../../asset/image/image12.png';
import Image3 from '../../asset/image/image13.png';
import Image4 from '../../asset/image/image14.png';
import Image5 from '../../asset/image/image15.png';



const Catalog = (props) => {
    const [total, setTotal] = useState("0");
    const [page, setPage] = useState("1")
    const [sort, setSort] = useState(1);
    const [namaBarang, setNamaBarang] = useState("");
    const [hargaBarang, setHargaBarang] = useState("");
    const [stokBarang, setStokBarang] = useState("");
    const [warnaBarang, setWarnaBarang] = useState("");
    const [imageBarang, setImageBarang] = useState();
    const [color, setColor] = useState("");


    const [onModal, setOnModal] = useState(false)
    const [onSort, setOnSort] = useState(false)
    const [resData, setResData] = useState(
        [
            {
                nama: "Product 1",
                kategori: [],
                purchasePrice: 10,
                harga: 20,
                image: Image1,
            },
            {
                nama: "Product 2",
                kategori: [],
                purchasePrice: 15,
                harga: 25,
                image: Image2,
            },
            {
                nama: "Product 3",
                kategori: [],
                purchasePrice: 20,
                harga: 30,
                image: Image3,
            },
            {
                nama: "Product 4",
                kategori: [],
                purchasePrice: 20,
                harga: 30,
                image: Image4,
            },
            {
                nama: "Product 5",
                kategori: [],
                purchasePrice: 20,
                harga: 30,
                image: Image5,
            },
            {
                nama: "Product 2",
                kategori: [],
                purchasePrice: 15,
                harga: 25,
                image: Image1,
            },
            {
                nama: "Product 3",
                kategori: [],
                purchasePrice: 20,
                harga: 30,
                image: Image2,
            },
            {
                nama: "Product 4",
                kategori: [],
                purchasePrice: 20,
                harga: 30,
                image: Image3,
            },
            {
                nama: "Product 5",
                kategori: [],
                purchasePrice: 20,
                harga: 30,
                image: Image4,
            },
        ]
    );

    const imageDummy = [Image1, Image2, Image3, Image4, Image5]

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 800,
        bgcolor: 'background.paper',
        borderRadius: "10px",
        boxShadow: 24,
        p: 4,
    };

    const styleSort = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 300,
        bgcolor: 'background.paper',
        borderRadius: "10px",
        boxShadow: 24,
        padding: "20px 40px",
    }

    useEffect(() => {
        loadProduct();
    }, [])


    const loadProduct = async () => {
        await axios.get(`https://646f0ae509ff19b1208674bc.mockapi.io/product`)
            .then(res => {
                if (res.data) {
                    var response = res.data
                    var totalRes = response.length
                    setResData(res.data)
                    setTotal(totalRes)
                    setPage(Math.ceil(totalRes / 9));
                    var index = 0;
                    var response = res.data.map((e) => {
                        if (index == 4) {
                            index = 0;
                        }
                        e.image = imageDummy[index];
                        index++;
                        return e;
                    })
                }
            })
    }

    const handleSort = (value) => {
        if (value == 3) {
            var newData3 = resData.sort(function (a, b) {
                return new Date(b.created_date) - new Date(a.created_date);
            });
            setResData(newData3);
            setSort(value)
        }
        else if (value == 2) {
            var newData2 = resData.sort(function (a, b) {
                return a.harga - b.harga;
            });
            setResData(newData2)
            setSort(value)
        } else if (value == 4) {
            var newData4 = resData.sort(function (a, b) {
                return b.harga - a.harga;
            });
            setResData(newData4)
            setSort(value)
        } else {
            loadProduct();
            var newData1 = resData;
            setResData(newData1)
            setSort(1)
        }
    }

    const showModal = (nama, harga, stok, warna, image) => {
        setOnModal(true)
        setNamaBarang(nama)
        setHargaBarang(harga)
        setStokBarang(stok)
        setWarnaBarang(warna)
        setImageBarang(image)
    }
    const handleClose = () => {
        setOnModal(false)
        setOnSort(false)
    };

    const showSort = () => {
        setOnSort(true)
    }

    return (
        <>
            <div style={{ padding: "35px 70px 35px 70px" }}>
                <Grid container>
                    <Grid item xs={2.5}>
                        <Filter setColor={setColor} />
                    </Grid>
                    <div style={{ width: "30px" }}></div>
                    <Grid item xs={9}>
                        <img alt="banner" style={{ width: 992, height: 206 }} src={props.type == "PRIA" ? Banner : props.type == "WANITA" ? BannerWoman : BannerKids} />
                        <Grid container style={{ margin: "40px 0" }}>
                            <Grid item xs={3}>
                                <span style={{ fontWeight: "bold" }}>KAOS {props.type}</span>
                            </Grid>
                            <Grid item xs={2}>
                                <span>{total} Produk</span>
                            </Grid>
                            <Grid item xs={7}>
                                <Button onClick={() => showSort()} style={{ backgroundColor: "#323232", color: "white", float: "right" }}>Urutkan <SortAscendingOutlined /></Button>
                            </Grid>
                        </Grid>
                        <Grid container style={{ display: "grid", gridTemplateColumns: "1fr 1fr 1fr" }}>
                            {
                                resData ? color ?
                                    resData.filter(data => data.warna == color).map(filteredWarna => (
                                        <div onClick={() => showModal(filteredWarna.nama, filteredWarna.harga, filteredWarna.stok, filteredWarna.warna, filteredWarna.image)} style={{ display: "flex", flexDirection: "column", margin: "0 30px 20px 0" }} >
                                            <img src={filteredWarna.image} alt={filteredWarna.nama} />
                                            <Grid container>
                                                <Grid item xs={8}>
                                                    <p style={{ fontWeight: "bold" }}>{filteredWarna.nama}</p>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <p style={{ float: "right" }}>Rp. {filteredWarna.harga}</p>
                                                </Grid>
                                            </Grid>
                                        </div>
                                    ))

                                    :
                                    resData.map((data, index) => (
                                        <div onClick={() => showModal(data.nama, data.harga, data.stok, data.warna, data.image)} style={{ display: "flex", flexDirection: "column", margin: "0 30px 20px 0" }} key={index}>
                                            <img src={data.image} alt={data.nama} />
                                            <Grid container>
                                                <Grid item xs={8}>
                                                    <p style={{ fontWeight: "bold" }}>{data.nama}</p>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <p style={{ float: "right" }}>Rp. {data.harga}</p>
                                                </Grid>
                                            </Grid>
                                        </div>
                                    ))
                                    :
                                    resData.map((data, index) => (
                                        <div style={{ display: "flex", flexDirection: "column", margin: "0 30px 20px 0" }} key={index}>
                                            <Skeleton.Image active={true} style={{ width: 310, height: 360 }} />
                                            <br />
                                            <Skeleton.Input active={true} size={20} />
                                        </div>
                                    ))
                            }


                        </Grid>
                        <br />
                        {
                            resData ?
                                <Pagination
                                    total={total}
                                    showTotal={(total, range) => `${range[1]}-${range[1]} of ${total} items`}
                                    defaultPageSize={10}
                                    defaultCurrent={1}
                                />
                                : null
                        }

                    </Grid>
                </Grid>
            </div>

            <Modal
                open={onModal}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Grid container >
                        <Grid item xs={8} style={{ display: "grid", gridTemplateColumns: "1fr 1fr" }}>
                            <img src={imageBarang} style={{ width: "200px", height: "250px" }} />
                            <img src={imageBarang} style={{ width: "200px", height: "250px" }} />
                            <img src={imageBarang} style={{ width: "200px", height: "250px" }} />
                            <img src={imageBarang} style={{ width: "200px", height: "250px" }} />

                        </Grid>
                        <Grid item xs={4}>
                            <Typography>{namaBarang}</Typography><br></br>
                            <Typography mt={-2} style={{ fontWeight: "bold" }}>
                                Rp. {hargaBarang}
                            </Typography>
                            <Typography mt={4}>Warna</Typography><br></br>
                            <Typography mt={-2} style={{ fontWeight: "bold" }}>
                                {warnaBarang}
                            </Typography>
                            <Typography mt={4}>stok</Typography><br></br>
                            <Typography mt={-2} style={{ fontWeight: "bold" }}>
                                {stokBarang}
                            </Typography>
                            <Typography mt={4}>Ukuran</Typography><br></br>
                            <Typography mt={-2} style={{ fontWeight: "bold", display: "grid", gridTemplateColumns: "1fr 1fr 1fr 1fr 1fr", cursor: "pointer" }}>
                                <span>XS</span>
                                <span>S</span>
                                <span>M</span>
                                <span>L</span>
                                <span>XL</span>
                            </Typography>
                            <div style={{ marginTop: "10px" }}>
                                <Button style={{ backgroundColor: "#323232", color: "white", width: "80%" }}>Beli</Button><ShoppingCartOutlined style={{ fontSize: "20px" }} />
                            </div>

                        </Grid>
                    </Grid>
                </Box>
            </Modal>

            <Modal
                open={onSort}
                onClose={handleClose}
            >
                <Box sx={styleSort}>
                    <Typography id="modal-modal-title" variant="h6" component="h2" fontWeight={"bold"}>
                        Urutkan Berdasar
                    </Typography>
                    <Radio.Group name="radiogroup" defaultValue={sort} onChange={(e) => handleSort(e.target.value)}>
                        <Radio value={1}>Popularitas</Radio>
                        <Radio value={2}>Harga Terendah ke Termahal</Radio>
                        <Radio value={3}>Terbaru</Radio>
                        <Radio value={4}>Harga Termahal ke Terendah</Radio>
                        <Radio value={5}>Penjualan Terbaik</Radio>

                    </Radio.Group>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                    </Typography>
                </Box>
            </Modal>
        </>
    )
}

export default Catalog;
