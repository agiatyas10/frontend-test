import React, { useState, useEffect } from 'react';
import Navbar from "./layout/Navbar";
import Catalog from "./container/Catalog";

const App = () => {
  const [type, setType] = useState("PRIA");


  useEffect(() => {
    console.log(type, "type")
  },)

  return (
    <>
      <div>
        <Navbar setType={setType} />
        <Catalog type={type} />


      </div>
    </>
  );
}

export default App;
